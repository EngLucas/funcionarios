class Funcionario {
	private String nome;
	private	String empresa;
	private double salario;
	private	String entrada;
	private	String rg;
	private	boolean trabalhando;
	
	public void setNome (String umNome){
		this.nome = umNome;
	}

	public String getNome(){
		return nome;
	}
	
	public void setEmpresa (String umaEmpresa){
		this.empresa = umaEmpresa;
	}

	public String getEmpresa(){
		return empresa;	
	}
	
	public void setSalario (double umSalario){
		this.salario = umSalario;
	}
	
	public double getSalario (){
		return salario;	
	}

	public void setEntrada (String umaData){
		this.entrada = umaData;		
	}
	
	public String getEntrada (){
		return entrada;
	}
	
	public void setRg (String umRG){
		this.rg = umRG;
	}

	public String getRg (){
		return rg;
	}
	
	public void setTrabalhando (boolean estaOuNao){
		this.trabalhando = estaOuNao;
	}
	
	public boolean getTrabalhando(){
		if (trabalhando == true){
			return true;
		}else 	
			return false;
	}
	
	public void bonifica (double umDinheiro){
		this.salario = this.salario + umDinheiro;
	}

	public void demitido (){
		this.trabalhando = false;
	}
	
	public void mostra (){
		System.out.println("\nNome: " + getNome());
		System.out.println("Empresa: " + getEmpresa());
		System.out.println("Salario: " + getSalario());
		System.out.println("Data de Entrada: " + getEntrada());
		System.out.println("RG: " + getRg());
		System.out.println("Ativo na Empresa: " + getTrabalhando()+ "\n");
	
		
	
	}
	


}
